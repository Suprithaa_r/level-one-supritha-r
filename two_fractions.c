//WAP to find the sum of two fractions.
#include<stdio.h>

typedef struct 
{
	int n,d;
}fraction;

fraction get_values(int n)
{
    	fraction f;
    	printf("Enter Fraction %d\n",n);
    	printf("Numerator  :   ");
    	scanf("%d",&f.n);
    	printf("              ━━━\n");
    	printf("Denominator :  ");
    	scanf("%d",&f.d);
 	printf("\n");
    	return f;
}

int get_gcd(int num, int den)
{
	int gcd;
    	for(int i=1; i<=num && i<=den; i++)
	{
	 	if(num%i==0 && den%i==0)
		{
			gcd=i;
		}
	}
	return gcd;
}

fraction simplify(fraction sum)
{
	int gcd=get_gcd(sum.n,sum.d);
	sum.n=sum.n/gcd;
	sum.d=sum.d/gcd;
	return sum;
}


fraction compute_sum(fraction f1,fraction f2,fraction sum)
{
	    	
	sum.n=(f1.n*f2.d)+(f1.n*f2.d);
	sum.d=f1.d*f2.d;
    	sum=simplify(sum);
	return sum;
}

void display_sum(fraction f1,fraction f2,fraction sum)
{
	printf("Sum of %d/%d + %d/%d = %d/%d\n",f1.n,f1.d,f2.n,f2.d,sum.n,sum.d);
}
	
int main()
{
	fraction f1, f2, sum;
           	f1=get_values(1);
    	f2=get_values(2);
	sum=compute_sum(f1,f2,sum);
	display_sum(f1,f2,sum);
	return 0;
}

