//Write a program to find the sum of n different numbers using 4 functions
#include<stdio.h>
int input_n(int n)
{
	printf("Enter the number of elements\n");
	scanf("%d",&n);
	while(n<0)
	{
		printf("Enter positive number");
		scanf("%d",&n);
	}
	return n;
}
void input_array(int n, float array[n])
{
	for(int i=0;i<n;i++)
	{
	    printf("Enter the element no %d of the array\n",i+1);
	    scanf("%f",&array[i]);
    }
}
float find_sum(int n, float array[n])
{
	float sum=0;
	for(int i=0;i<n;i++)
	{
		sum=sum+array[i];
	}
    return sum;
}
void output(int n,float array[n],float sum)
{
    printf("sum=%.2f",sum);
}
int main()
{
	int n=input_n(n);
	float array[n];
	input_array(n,array);
	float sum=find_sum(n,array);
	output(n,array,sum);
	return 0;
}
