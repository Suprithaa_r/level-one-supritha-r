//Write a program to add two user input numbers using one function.
#include<stdio.h>
void add();
void main()
{
    add();
}
void add()
{
    int number1,number2,sum;
    printf("INPUT\n");
    printf("Enter number1:");
    scanf("%d", &number1);
    printf("Enter number2:");
    scanf("%d",&number2);
    sum= number1+number2;
    printf("OUTPUT\n");
    printf("Sum of %d and %d is %d", number1, number2, sum);
}
