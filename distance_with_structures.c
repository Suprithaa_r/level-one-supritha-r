//WAP to find the distance between two points using structures and 4 functions.#include<stdio.h>
#include<stdio.h>
#include <stdlib.h>
#include <math.h>

struct Point 
{
    int x, y;
};


float point_distance(struct Point a, struct Point b)
{
    float distance;
    distance = sqrt((a.x - b.x) * (a.x - b.x) + (a.y-b.y) *(a.y-b.y));
    return distance;
}

int main()
{
    struct Point a, b;
    printf("Enter coordinates (x1,y1): ");
    scanf("%d %d", &a.x, &a.y);
    printf("Enter coordinates (x2,y2): ");
    scanf("%d %d", &b.x, &b.y);
 printf("Distance between (%d,%d) x1,y1 and (%d,%d) x2,y2 = %f\n",a.x,a.y,b.x,b.y,point_distance(a,b));

    return 0;
}


